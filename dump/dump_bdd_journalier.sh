#!/bin/bash
db1="bdd1"
db2="bdd2"
my_port="port"


current_date=$(date +'%d-%m-%Y')
date_perime=$(date +%d-%m-%Y -d "$DATE - 14 day")

dump_path="/opt/dump_postgresql/journalier/"

for my_db in $db1 $db2 

do
    my_dump=$dump_path$my_db"_"$current_date".backup"
    my_dump_perime=$dump_path$my_db"_"$date_perime".backup"

    ##Suppresion du dump périmé
    if [ -f $my_dump_perime ]; then
        echo "Le dump : "$my_dump_perime" va être supprimé"
        rm $my_dump_perime
        ## Suppresion sur le ftp 
        ftp <<EOF
        open ftpback-rbx2-161.ovh.net
        cd dump/journalier
        mdelete $my_db"_"$date_perime".backup"
        bye
EOF
    else 
        echo "Pas de dump périmé a supprimer"
    fi 
    
    ## Création du dump du jour j 
    if [ -f $my_dump ]; then
        echo "Un dump existe déja"
    else 
        echo "Création du dump : "$my_dump

        pg_dump -f $my_dump -F c --dbname=$my_db --port=$my_port 
        #Copie du fichier sur le ftp 
        ftp <<EOF
        open ftpback-rbx2-161.ovh.net
        cd dump/journalier
        put $my_dump $my_db"_"$current_date".backup"
        bye
EOF
    fi 
done