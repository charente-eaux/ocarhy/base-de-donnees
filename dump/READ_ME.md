# Mise en place de sauvegarde automatique

## Les dumps 

- S'il y a plusieurs bases de données, chaque base dans un fichier séparé
- Dump global (pas de schema exclu)
- Copie des dumps vers le serveur FTP
- Format binaire pour la sauvegarde (.backup)


## Scripts a rendre éxécutable automatiquement, ils font : 

### Chaque jour : 

- [ ] Suppression du dump périmé => C'est à dire le dump qui a 14 jours (si il existe)
- [ ] Création du dump du jour J 

### Chaque mois : 

- [ ] Création d'un dump qui écrasse celui du même mois de l'année précédente

### Chaque année : 

- [ ] Création d'un dump le 1er janvier de chaque année (chaque année est conservé)

## Localisation des dumps 

### Sur le seveur 

- /opt/dump_postgresql/journalier
- /opt/dump_postgresql/mensuel
- /opt/dump_postgresql/annuel

### Sur le ftp 

*A la racine du FTP*

- dump/journalier
- dump/mensuel
- dump/annuel

**Pour éviter de de devoir écrire les nom d'utilisateur et mot de passe en dur dans les scripts on vba passé par un fichier de config du ftp : ```.netrc```**

### Lancement 

- Le dump journalier est éxécuté tous les jours à 1h10
- Le dump mensuel tous les 1er de chaque mois à 2h20
- Le dump annuel tous les 1er janvier de chaque année à 3h30

## Les scripts

### Script **journalier** : dump_bdd_journalier.sh : 

### Script **mensuel** : dump_bdd_mensuel.sh : 

### Script **annuel** : dump_bdd_annuel.sh : 


# Mise en place de l'automatisation


## Créer le dossier de destination des dumps et affecter les droits au user postgres 

```shell
cd /opt/

sudo mkdir dump_postgresql
sudo chown postgres:postgres dump_postgresql

cd dump_postgresql

sudo mkdir journalier
sudo chown postgres:postgres journalier

sudo mkdir mensuel
sudo chown postgres:postgres mensuel

sudo mkdir annuel
sudo chown postgres:postgres annuel
```


## Mettre ce script dans un endroit ou postgres peux le lire 

```shell
cd /usr/local/bin
sudo mkdir dump 
cd dump
```
```shell
sudo nano dump_bdd_journalier.sh
```
*Insérer le script journalier*
```shell
sudo nano dump_bdd_mensuel.sh
```
*Insérer le script mensuel*

```shell
sudo nano dump_bdd_annuel.sh
```
*Insérer le script annuel*

## Rendre les scripts éxécutable 

```shell
sudo chmod +x dump_bdd_journalier.sh
sudo chmod +x dump_bdd_mensuel.sh
sudo chmod +x dump_bdd_annuel.sh
```



## Ajouter un fichier de configuration pour la connexion au ftp 

Cela évite de devoir écrire en dur les identifiants du ftp dans le script 

### Se déplacer dans le home de postgres
```
cd /var/lib/postgresql
```

### Créer le fichier de config
```
sudo nano .netrc
```
### Ecrire cette ligne
```
machine ADRESSE_DU_SERVER login LE_USER password LE_MDP
```

### Définir postgres comme prorpirètaire de ce fichier 
```
sudo chown postgres:postgres .netrc
```

### Donner des droits de lecture uniquement à Postgres 
```
sudo chmod 0600 .netrc
```

## Si besoin de tester les scripts (optionnel)
```
sudo -u postgres sh /usr/local/bin/dump/dump_bdd_journalier.sh
sudo -u postgres sh /usr/local/bin/dump/dump_bdd_mensuel.sh
sudo -u postgres sh /usr/local/bin/dump/dump_bdd_annuel.sh
```

## Si besoin de modifier les scripts (optionnel)
```
sudo nano sh /usr/local/bin/dump/dump_bdd_journalier.sh
sudo nano sh /usr/local/bin/dump/dump_bdd_mensuel.sh
sudo nano sh /usr/local/bin/dump/dump_bdd_annuel.sh
```


## Automatisation cron : modifier le fichier des tâches planifier 

### Editer les crons avec le user postgres

```sh
sudo crontab -e -u postgres
```

- -e: C'est une option de la commande crontab qui spécifie l'édition du fichier de configuration cron. Cela ouvrira le fichier de crontab associé à l'utilisateur pour édition.
- -u postgres: C'est une option de la commande crontab qui spécifie l'utilisateur pour lequel vous souhaitez éditer le fichier cron. Dans ce cas, il indique que les modifications seront apportées au fichier cron de l'utilisateur "postgres".

### Ajouter les lignes de cron

**Attention le serveur c'est pas a l'heure de Paris GMT+1 mais à GMT**

```cron
10 1 * * * sh /usr/local/bin/dump/dump_bdd_journalier.sh > /opt/dump_postgresql/journalier/dump_journalier.log

20 2 1 * * sh /usr/local/bin/dump/dump_bdd_mensuel.sh > /opt/dump_postgresql/mensuel/dump_mensuel.log

30 3 1 1 * sh /usr/local/bin/dump/dump_bdd_annuel.sh > /opt/dump_postgresql/annuel/dump_annuel.log
```

- Le premier champ (10) représente la minute. Dans ce cas, la tâche est planifiée pour s'exécuter à la 10e minute de l'heure.
- Le deuxième champ (1) représente l'heure. La tâche est planifiée pour s'exécuter à 1 heure du matin.
- Les astérisques dans les champs restants () représentent "chaque" ou "n'importe quel". Par conséquent, la tâche est planifiée pour s'exécuter tous les jours du mois, tous les mois et tous les jours de la semaine.

### Visualiser les tâches planifiés 

```sh
sudo crontab -l -u postgres
```

> La sauvegarde est en place



### Si changement de port 

- Mettre à jour le port dans les dumps
- Mettre à jour dans le la config de serveur