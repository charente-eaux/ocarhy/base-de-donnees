#!/bin/bash
db1="bdd1"
db2="bdd2"
my_port="port"

current_date=$(date +'%Y')

dump_path="/opt/dump_postgresql/annuel/"

for my_db in $db1 $db2 

do
    my_dump=$dump_path$my_db"_"$current_date".backup"

    ## Création du dump annuel 
    pg_dump -f $my_dump -F c --dbname=$my_db --port=$my_port 

    #Copie du fichier sur le ftp 
    ftp <<EOF
    open ftpback-rbx2-161.ovh.net
    cd dump/annuel
    put $my_dump $my_db"_"$current_date".backup"
    bye
EOF

done
